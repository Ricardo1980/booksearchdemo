//
//  Book.m
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "Book.h"
#import "Author.h"

@implementation Book

-(NSString*) description {
    return [NSString stringWithFormat:@"Title: %@\nPublisher: %@\nLanguage: %@\nAuthor(s): %@",
            self.title,
            self.publisherName?self.publisherName:@"N/A",
            self.language?self.language:@"N/A",
            [self processAuthorNames]];
}

-(NSString*) processAuthorNames {
    if (self.authors.count==0) {
        return @"N/A";
    }
    NSMutableArray *authorNames=[NSMutableArray new];
    for (Author *author in self.authors) {
        if (author.name) {
            [authorNames addObject:author.name];
        }
    }
    if (authorNames.count==0) {
        return @"N/A";
    }
    return [authorNames componentsJoinedByString:@", "];
}

@end