//
//  BooksTableViewController.h
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BooksTableViewController : UITableViewController
@property (nonatomic, strong) NSArray *books;
@end
