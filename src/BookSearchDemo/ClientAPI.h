//
//  ClientAPI.h
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClientAPI : NSObject
-(void) searchBooksByName:(NSString*)name onSuccess:(void(^)(NSArray *books))success onFail:(void(^)(NSError *error))fail;
@end
