//
//  Book.h
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Book : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *publisherName;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSArray *authors;
@end
