//
//  ClientAPI.m
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "ClientAPI.h"
#import <Restkit.h>
#import "Book.h"
#import "Author.h"

static NSString* const kUrlBase=@"http://isbndb.com/api/v2/json/U3W26R0N";
static NSString* const kBooksPath=@"books";

@interface ClientAPI()
@property (strong, nonatomic) RKObjectManager *objectManager;
@end

@implementation ClientAPI

-(id) init {
    
    self=[super init];
    if (self) {
        
        // restkit
        /*
         RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
         RKLogConfigureByName("RestKit", RKLogLevelDefault);
         RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelDefault);
         */
        self.objectManager=[RKObjectManager managerWithBaseURL:[NSURL URLWithString:kUrlBase]];
        
        // workaround to fix a problem with the web service in the content type
        [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/plain"];
        
        
    
        // author mapping
        RKObjectMapping *authorMapping = [RKObjectMapping mappingForClass:Author.class];
        [authorMapping addAttributeMappingsFromDictionary:@{@"id": @"authorId",
                                                              @"name": @"name"}];
    
        // book mapping
        RKObjectMapping *bookMapping = [RKObjectMapping mappingForClass:Book.class];
        [bookMapping addAttributeMappingsFromDictionary:@{@"title": @"title",
                                                              @"publisher_name": @"publisherName",
                                                              @"language": @"language"}];
        [bookMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"author_data"
                                                                                    toKeyPath:@"authors"
                                                                                  withMapping:authorMapping]];

        // books response
        RKResponseDescriptor *booksResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:bookMapping method:RKRequestMethodGET pathPattern:kBooksPath keyPath:@"data" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
        [self.objectManager addResponseDescriptor:booksResponseDescriptor];
    }
    return self;
}


-(void) searchBooksByName:(NSString*)name onSuccess:(void(^)(NSArray *books))success onFail:(void(^)(NSError *error))fail {
    
    if (!name) {
        NSError* error=[NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:@"There must be a name."}];
        if (fail) fail(error);
        return;
    }
    
    NSDictionary* params=@{@"q": name};
    RKObjectRequestOperation *requestOperation = [self.objectManager appropriateObjectRequestOperationWithObject:nil method:RKRequestMethodGET path:kBooksPath parameters:params];
    
    [requestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        NSArray *books=mappingResult.array;
        if (success) success(books);
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (fail) fail(error);
    }];
    [self.objectManager enqueueObjectRequestOperation:requestOperation];
}

@end
