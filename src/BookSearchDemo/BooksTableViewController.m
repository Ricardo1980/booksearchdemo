//
//  BooksTableViewController.m
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "BooksTableViewController.h"
#import "Book.h"
#import "BookDetailViewController.h"

@interface BooksTableViewController()
@property (strong, nonatomic) Book *selectedBook;
@end

@implementation BooksTableViewController

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.books.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BookCell" forIndexPath:indexPath];
    Book *book=self.books[indexPath.row];
    cell.textLabel.text=book.title;
    cell.detailTextLabel.text=book.publisherName;
    return cell;
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:NSStringFromClass(BookDetailViewController.class)]) {
        BookDetailViewController *vc=(BookDetailViewController*)segue.destinationViewController;
        vc.book=self.selectedBook;
    }
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedBook=self.books[indexPath.row];
    [self performSegueWithIdentifier:NSStringFromClass(BookDetailViewController.class) sender:self];
}

@end
