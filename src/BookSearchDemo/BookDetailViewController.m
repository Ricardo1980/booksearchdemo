//
//  BookDetailViewController.m
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "BookDetailViewController.h"
#import "Book.h"

@interface BookDetailViewController()
@property (weak, nonatomic) IBOutlet UITextView *bookDetailTextView;
@end

@implementation BookDetailViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    self.bookDetailTextView.text=[self.book description];
}

@end
