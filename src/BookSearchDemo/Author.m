//
//  Author.m
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "Author.h"

@implementation Author

-(NSString*) description {
    return self.name;
}

@end
