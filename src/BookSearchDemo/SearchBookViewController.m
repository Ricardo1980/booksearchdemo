//
//  SearchBookViewController.m
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "SearchBookViewController.h"
#import "ClientAPI.h"
#import "BooksTableViewController.h"

@interface SearchBookViewController()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) ClientAPI *clientAPI;
@property (strong, nonatomic) NSArray *retrievedBooks;
@end

@implementation SearchBookViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    self.activityIndicatorView.hidden=YES;
    self.clientAPI=[ClientAPI new];
    [self.searchTextField becomeFirstResponder];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:NSStringFromClass(BooksTableViewController.class)]) {
        BooksTableViewController *vc=(BooksTableViewController*)segue.destinationViewController;
        vc.books=self.retrievedBooks;
    }
}

- (IBAction)onSearchButtonPressed:(UIButton *)sender {
    
    // there must be something to search
    if (self.searchTextField.text.length==0) {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Write something" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }

    [self.activityIndicatorView startAnimating];
    sender.enabled=NO;
    
    // ask the api about the books with that name
    __weak typeof(self) weakSelf=self;
    [self.clientAPI searchBooksByName:self.searchTextField.text onSuccess:^(NSArray *books) {
        sender.enabled=YES;
        [weakSelf.activityIndicatorView stopAnimating];
        weakSelf.retrievedBooks=books;
        [weakSelf performSegueWithIdentifier:NSStringFromClass(BooksTableViewController.class) sender:weakSelf];

    } onFail:^(NSError *error) {
        sender.enabled=YES;
        [weakSelf.activityIndicatorView stopAnimating];
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }];
}

@end
