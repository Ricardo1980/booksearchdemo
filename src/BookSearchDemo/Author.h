//
//  Author.h
//  BookSearchDemo
//
//  Created by Ricardo on 14/9/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Author : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *authorId;
@end
