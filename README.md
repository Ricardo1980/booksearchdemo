The app is very simple.
Uses RestKit for networking and data model mapping.

Usually I add a data model class and inside that one, the client api that hides networking.
In this case I pass the books to the table view controller instead of using a data model class.
Same thing for the book detail with the Book instance.

It can be done in the 2 ways.

About networking, here I use Restkit to save time. In other projects I use NSURLSession directly., like: https://bitbucket.org/Ricardo1980/weatherdemo

I didn't add testing or a nice UI, basically because there is no time.

The app uses cocoapods, so you have to execute 'pod update'